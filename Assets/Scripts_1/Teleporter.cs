﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Teleporter : MonoBehaviour
{
    public float offset;
    GameObject player;
    float rotation;
    Rigidbody2D rb2d;

    void Start()
    {
        player = GameObject.Find("Player");
        rb2d = GetComponent<Rigidbody2D>();
        Vector3 difference = Camera.main.ScreenToWorldPoint(Input.mousePosition) - transform.position;                  //Dieser Code wurde prinzipiell nicht von mir geschrieben. Ich habe ihn aus dem Internet übernommen und nutze ihn inwzischen seit über einem Jahr, da er perfekt funktioniert und ich delay prinzipiell selber durch z.B. eine Lerp Funktion hinzufügen kann.
        rotation = Mathf.Atan2(difference.y, difference.x) * Mathf.Rad2Deg;

        transform.rotation = Quaternion.Euler(0f, 0f, rotation + offset);
    }

    
    void Update()
    {
        if (Input.GetMouseButtonUp(0)) telep();
        rb2d.velocity = transform.right * 15;
    }

    void telep()
    {
        player.transform.position = transform.position;
        Destroy(gameObject);

    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        telep();
    }
}

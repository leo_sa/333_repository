﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Perks : MonoBehaviour
{
    public int perkA, perkB, perkC;
    Rigidbody2D rb2d;
    PlayerMovement pm;
    public bool canDash = true, instanTele = true;
    public GameObject hook, teleporter;

    private void Start()
    {
        pm = FindObjectOfType<PlayerMovement>();
        rb2d = GetComponent<Rigidbody2D>();
    }


    void Update()
    {
        switch (perkA)
        {
            case 0:
                teleporterInstantiate();
                break;
        }

        switch (perkB)
        {
            case 0:
                dash();
                break;

        }

        switch (perkC)
        {

        }
    }



    void dash()
    {
        if (Input.GetMouseButtonDown(1) && canDash)
        {
            rb2d.MovePosition(transform.position + transform.right * pm.force * 2);
            canDash = false;
        }
    }

    void teleporterInstantiate()
    {
        if (Input.GetMouseButtonDown(0) && instanTele)
        {
            Instantiate(teleporter, transform.position, Quaternion.identity);
            instanTele = false;
        }
    }
}

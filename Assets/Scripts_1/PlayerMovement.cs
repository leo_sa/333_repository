﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
    Rigidbody2D rb2d;
    public float accelerationTime, Beginspeed, maxSpeed, acceleration, gravityacceleration, gravi, maxGrav, vertSpeed, jumpAccelTime, gravityAcceleration;
    float speed, timer, jumpTimer, jump, jumpH, sMulti;
    [HideInInspector]
    public int force, collisions;
    bool grounded, left, right;


    void Start()
    {
        speed = Beginspeed;
        rb2d = GetComponent<Rigidbody2D>();
    }


    void Update()
    {
        force = (int)Input.GetAxisRaw("Horizontal");
        if (!grounded) rb2d.gravityScale *= gravityacceleration;
        else rb2d.gravityScale = gravi;
        if (rb2d.gravityScale >= maxGrav) rb2d.gravityScale = maxGrav;

        if (Input.GetKeyDown("s") && collisions == 0) sMulti = gravityAcceleration;
        else if (Input.GetKeyUp("s") || collisions != 0) sMulti = 0;
        rb2d.AddForce(Vector2.up * -sMulti, ForceMode2D.Impulse);

        if (force != 0)
        {

            if (timer <= accelerationTime)
            {
                timer += Time.deltaTime;
                if (speed < maxSpeed) speed += acceleration * Time.deltaTime;
                else speed = maxSpeed;
            }
        }
        else
        {
            timer = 0;
            speed = 0;
        }

        if ((Input.GetKey("w") || Input.GetKey(KeyCode.Space)) && grounded)
        {
            if (!left && !right)
            {
                jumpH = 0;
                if (jumpTimer <= jumpAccelTime)
                {
                    jumpTimer += Time.deltaTime;
                    jump = vertSpeed;
                }
                else
                {
                    grounded = false;
                    jump = 0;
                }
            }

        }
        else if (Input.GetKeyUp("w") || Input.GetKeyUp(KeyCode.Space))
        {
            grounded = false;
            jumpTimer = 0;
            jump = 0;
        }


        rb2d.AddForce(Vector2.up * jump, ForceMode2D.Impulse);                //Jump
        rb2d.velocity = new Vector2(speed * force, rb2d.velocity.y);
    }





    private void OnCollisionEnter2D(Collision2D collision)
    {
        collisions++;
        grounded = true;

    }


    private void OnCollisionExit2D(Collision2D collision)
    {
        collisions--;
    }


}
